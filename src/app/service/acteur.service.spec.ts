import { TestBed, inject } from '@angular/core/testing';

import { ActeurService } from './acteur.service';

describe('ActeurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActeurService]
    });
  });

  it('should be created', inject([ActeurService], (service: ActeurService) => {
    expect(service).toBeTruthy();
  }));
});
