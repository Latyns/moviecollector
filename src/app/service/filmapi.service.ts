import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MOVIE_API_URL} from '../constants';
import {Acteur} from '../class/acteur';
import {Observable} from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json', 'Acces-Control-Allow-Origin' : '*'})
};

@Injectable({
  providedIn: 'root'
})

export class FilmapiService {
  private ressourceUrl = MOVIE_API_URL + 'films';

  constructor(private http: HttpClient) { }

  public create(acteur: Acteur) {
    return this.http.post<Acteur>(this.ressourceUrl, acteur, httpOptions);
  }
  public getOne(id: number): Observable<Acteur> {
    return this.http.get<Acteur>(`${this.ressourceUrl}/${id}`);
  }
  public getAll(): Observable<Acteur[]> {
    return this.http.get<Acteur[]>(this.ressourceUrl);
  }
  public deleteActeur(id: number): Observable<{}> {
    const url = `${this.ressourceUrl}/${id}`;
    return this.http.delete(url, httpOptions);
  }
  public modificationActeur(id: number) {
    return this.http.put<Acteur>(`${this.ressourceUrl}/${id}`, httpOptions);
  }

}
