import { TestBed, inject } from '@angular/core/testing';

import { FilmapiService } from './filmapi.service';

describe('FilmapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilmapiService]
    });
  });

  it('should be created', inject([FilmapiService], (service: FilmapiService) => {
    expect(service).toBeTruthy();
  }));
});
