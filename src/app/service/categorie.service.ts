import { Injectable } from '@angular/core';
import {MOVIE_API_URL} from '../constants';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Categorie} from '../class/categorie';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json', 'Acces-Control-Allow-Origin' : '*'})
};


@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  private ressourceUrl = MOVIE_API_URL + 'categories';

  constructor(private http: HttpClient) { }

  public create(categorie: Categorie) {
    return this.http.post<Categorie>(this.ressourceUrl, categorie, httpOptions);
  }
  public getOne(id: number): Observable<Categorie> {
    return this.http.get<Categorie>(`${this.ressourceUrl}/${id}`);
  }
  public getAll(): Observable<Categorie[]> {
    return this.http.get<Categorie[]>(this.ressourceUrl);
  }
  public deleteCategorie(id: number): Observable<{}> {
    const url = `${this.ressourceUrl}/${id}`;
    return this.http.delete(url, httpOptions);
  }
  public modificationCategorie(id: number) {
    return this.http.put<Categorie>(`${this.ressourceUrl}/${id}`, httpOptions);
  }
}
