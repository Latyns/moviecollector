import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FilmsComponent } from './films/films.component';
import { CategoriesComponent } from './categories/categories.component';
import { ActeursComponent } from './acteurs/acteurs.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'home'},
  { path: 'home',                        component: HomeComponent },
  { path: 'films',                       component: FilmsComponent },
  { path: 'acteurs',                     component: ActeursComponent },
  { path: 'categories',                  component: CategoriesComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FilmsComponent,
    CategoriesComponent,
    ActeursComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
