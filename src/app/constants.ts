import { environment } from '../environments/environment';



const _MOVIE_API_URL = environment.api;

/* @toreplace VERSION */
/* @toreplace DEBUG_INFO_ENABLED */
/* @toreplace SERVER_API_URL */
/* tslint:enable */

export const MOVIE_API_URL       = _MOVIE_API_URL;
