import { Component, OnInit } from '@angular/core';
import {Acteur} from '../class/acteur';
import {ActeurService} from '../service/acteur.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-acteurs',
  templateUrl: './acteurs.component.html',
  styleUrls: ['./acteurs.component.css'],
  providers: [ActeurService]
})
export class ActeursComponent implements OnInit {
  public acteurs: Acteur[];
  public selectedActeur: Acteur;
  public actors;


  constructor(public acteurService: ActeurService,  private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.acteurService.getAll().subscribe(
      (result) => this.acteurs = result
    );
  }
  getActeur(acteur: Acteur): void {
    this.selectedActeur = acteur;
  }
  supprimer(idActeur: number) {
    this.acteurService.deleteActeur(idActeur).subscribe();
    console.log(idActeur);
    location.reload();
  }
  // putActeur(acteur: Acteur): void {
  //   this.selectedActeur = acteur;
  //   this.acteurService.modificationActeur(this.selectedActeur).subscribe(
  //     acteurData => {
  //       this.selectedActeur = acteurData;
  //       console.log(this.selectedActeur);
  //     });
  // }
  // onSubmit(acteurForm: NgForm) {
  //   console.log(acteurForm.value);
  // }


}
