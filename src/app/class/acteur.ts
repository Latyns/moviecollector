import {Film} from './film';

export class Acteur {
  id: number;
  nom: string;
  prenom: string;
  films: Film[];
}
