import {Categorie} from './categorie';
import {Acteur} from './acteur';

export class Film {
  id: number;
  annee: number;
  poster: string;
  synopsis: string;
  categorie: Categorie;
  acteurs: Acteur[];
}
