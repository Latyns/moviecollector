# API Doc

## Acteurs

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /acteurs   | GET |  | 200: ``` [ { "id": 42, "name": "John", "surname": "Doe", {..}] ``` |
| /acteurs   | POST      |   *required* body ``` {"name": "John", "surname": "Doe" } ``` | 201: ``` { "id": 42, "name": "John","surname": "Doe", "films": [ {"id": 42, "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film"}]} ``` |
| /acteurs/{id}   | GET      |     | 200: ``` { "id": 42, "name": "John", "surname": "Doe", "film": [ {"id": 42, "title": "Titre de l'article"}]} ``` |
| /acteurs/{id}   | DELETE      |     | 200 ``` {} ``` |
| /acteurs/{id}   | PUT      |  *required* body ``` {"name": "John", "surname": "Doe" } ```   | 200: ``` { "id": 42, "name": "John", "surname": "Doe" }``` |



## Films

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /Films   | GET |  | 200: ``` [ { {"id": 1, "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", author: { "id": 42, "name": "John", email: "email@domain.net"}, {..} ] ``` |
| /Films   | POST      |   *required* body ``` {" "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", "author": 42, "category": 1  } ``` | 201: ``` { "id": 1, "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", actor: { "id": 42, "name": "John", "surname": "Doe"} }``` | 200: ``` {  "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", actor: { "id": 42, "name": "John", "surname": "Doe"} }``` |
| /Films/{id}   | GET      |     | 200: ``` { "id": 42, "title": "Titre", "description": "Description de l'article...", actor: { "id": 42, "name": "John", "surname": "Doe", {..} }``` |
| /Films/{id}   | DELETE      |     | 200 ``` {} ``` |
| /Films/{id}   | PUT      |  *required* body ``` {"title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", "actor": 42, "category": 1  } ```   | 200: ``` { "id": 42, "title": "titre du film", "year": annee du film, "poster": posteur du film, "synopsis": "description du film", "author": 42 }``` |


## Category

| URI        | Method           | Params  | Response |
| ---------- |:-------------:| -----:| --- |
| /categories   | GET |  | 200: ``` [ { "id": 42, "name": "Nom"}, {..} ] ``` |
| /categories   | POST      |   *required* body ``` {"name": "Titre"} ``` | 201: ``` { "id": 42, "name": "Nom"}``` |
| /categories/{id}   | GET      |     | 200: ``` { "id": 42, "name": "Nom"}``` |
| /categories/{id}   | DELETE      |     | 200 ``` {} ``` |
| /categories/{id}   | PUT      |  *required* body ``` {"name": "Titre"} ```   | 200: ``` { "id": 42, "name": "Nom"}``` |
